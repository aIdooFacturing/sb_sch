var prog_run = false;
onmessage = function(evt){
	var data = evt.data;
	prog_run = true;
	if(data=="done"){
		prog_run = false;
	}else{
		sendData(data);
	};
};

function sendData(data){
	postMessage(data);
	if(prog_run){
		setTimeout(function(){
			sendData(data);
		}, 100)
	};
};