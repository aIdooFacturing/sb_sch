//machineList id, company, name, comIp, x, y
var cPage = 1;
var panel = false;
var colors, colors_a;
var subMenuTr = false;
var status3, status4;
var circleWidth = contentWidth * 0.07;
var canvas;
var ctx;
var dvcName = [];

$(function() {
	canvas = document.getElementById("canvas");
	canvas.width = contentWidth;
	canvas.height = contentHeight;

	ctx = canvas.getContext("2d");
	
	//$("#comName").html(window.sessionStorage.getItem("company"));
	setEl();
	//drawLine();
	setToday();
	getAllMachine();
	
	drawLineChart("lineChart");
	
	evtBind();
	$("#panel_table tr:nth(1) td:nth(0) img").css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	$("#diagram").circleDiagram({
		textSize: getElSize(70), // text color
		percent : "68.5%",
		size: getElSize(400), // graph size
		borderWidth: getElSize(30), // border width
		bgFill: "#95a5a6", // background color
		frFill: "#1abc9c", // foreground color
		//font: "serif", // font
		textColor: 'white' // text color
	});
	
	setTimeout(resetIcon, 3000);
	thermometer();
	
	getReportBarData();
});

function setSlideMode(){
	if(auto_flag){
		alert("Mode : Manual");
		auto_flag = false;
	}else{
		alert("Mode : Auto");
		slideIdx = 6;
		auto_flag = true;
		autoSlide();
	};
	return false;
};

var slideOrder = [
                  	["$('.machine_cam').not('.video').eq(0).click()", 10000],
                  	["$('.machine_cam').not('.video').eq(0).click()", 10000],
                  	["$('.icon:nth(3)').click()", 10000],
                  	["flipCard_r()", 10000],
                  	["$('#menu_btn').click()", 10000],
                  	["$('#panel_table td img:nth(1)').click()", 10000],
                  	["$('#menu_btn').click()", 10000],
                  	["$('#panel_table td img:nth(0)').click()", 10000],
                  ];
var slideIdx = 0;
function autoSlide(){
	if(auto_flag) {
//		if(slideIdx==slideOrder.length){
//			location.reload();
//		};
		
		setTimeout(function(){
			eval(slideOrder[slideIdx][0]);
			
			slideIdx++;
			if(slideIdx==slideOrder.length) location.reload(); //slideIdx = 0;
			autoSlide();
		},slideOrder[slideIdx][1]);
	}
};

var auto_flag = true;
function getReportBarData(){
	 var url = ctxPath + "/chart/getReportBarData.do";
	 var sDate = $("#sDate").val();
	 var eDate = $("#eDate").val();
	 var param = "sDate=" + sDate + 
	 			"&eDate=" + eDate;
	 
	 $.ajax({
		url : url,
		type : "post",
		data : param,
		dataType : "json",
		success  : function(data){
			
		}
	 });
};

function drawLine(x1,y1,x2,y2){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	
	$("#sDate, #eDate").val(year + "-" + month + "-" + day);
};

//originally from http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript
function formatCurrency(n, c, d, t) {
    "use strict";

    var s, i, j;

    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    s = n < 0 ? "-" : "";
    i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "";
    j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function thermometer(goalAmount, progressAmount, animate) {
    "use strict";

    var $thermo = $("#thermometer"),
        $progress = $(".progress", $thermo),
        $goal = $(".goal", $thermo),
        percentageAmount;

    goalAmount = goalAmount || parseFloat( $goal.text() ),
    progressAmount = progressAmount || Number( $progress.text() ),
    percentageAmount =  Math.min( Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

    //let's format the numbers and put them back in the DOM
    $goal.find(".amount").text(formatCurrency( goalAmount ) );
    $progress.find(".amount").text(progressAmount + "°C");


    //let's set the progress indicator
    $progress.find(".amount").hide();
    if (animate !== false) {
        $progress.animate({
            "height": percentageAmount + "%"
        }, 1200, function(){
            $(this).find(".amount").fadeIn(500);
        });
    }else{
        $progress.css({
            "height": percentageAmount + "%"
        });
        $progress.find(".amount").fadeIn(500);
    };
};

function drawLineChart(id){
	$('#' + id).highcharts({
		chart : {
			height : getElSize(300),
			backgroundColor : "rgba(0,0,0,0)",
			marginBottom : 0
		},
		exporting : false,
		credits : false,
        title: {
            text: false,
        },
        subtitle: {
            text: false,
        },
        xAxis: {
        	lineWidth: 0,
        	minorGridLineWidth: 0,
        	minorTickLength: 0,
        	tickLength: 0,
        	lineColor: 'transparent',
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0
        },
        yAxis: {
        	labels : {
        		enabled : false
        	},
        	gridLineWidth: 0,
            title: {
                text: false
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions : {
        	series : {
        		dataLabels: {
                    enabled: true,
                    style : {
                    	fontSize : getElSize(30),
                    	color : "white",
                    	textShadow: 0
                    },
                    formatter: function () {
                        if (this.point.options.showLabel) {
                            return this.y;
                        }
                        return null;
                    }
                },
            	lineWidth : getElSize(10),
            	marker: {
                    lineWidth: getElSize(10),
                    lineColor: null // inherit from series
                }
            },
        },
        tooltip: {
            //valueSuffix: '°C'
        },
        legend: {
        	enabled : false
        },
        series: []
    });
	
	var chart = $("#lineChart").highcharts()
	var data = Number(Number(Math.random() * 10).toFixed(1));
	chart.addSeries({data:[data], name : "Data"});
	
	for(var i = 0; i < 9; i++){
		data = Number(Number(Math.random() * 10).toFixed(1));
	 	chart.series[0].addPoint(data);
	 	$("#lineChartLabel").html(data);
	};
	
	callback();
};

function callback(){
	var chart = $("#lineChart").highcharts();
	var series = chart.series[0];
    var points = series.points;
    var pLen = points.length;
    var i = 0;
    var lastIndex = pLen - 1;
    var minIndex = series.processedYData.indexOf(series.dataMin);
    var maxIndex = series.processedYData.indexOf(series.dataMax);

    points[minIndex].options.showLabel = true;
	points[maxIndex].options.showLabel = true;
	//  points[lastIndex].options.showLabel = true;
	series.isDirty = true;
	chart.redraw();
};

function drawReportColumnChart(id){
	Highcharts.createElement('link', {
		   href: '//fonts.googleapis.com/css?family=Unica+One',
		   rel: 'stylesheet',
		   type: 'text/css'
		}, null, document.getElementsByTagName('head')[0]);
	
	 $('#' + id).highcharts({
		 	chart : {
		 		height : getElSize(1100),
		 		type: 'column',
		 		backgroundColor : "rgba(0,0,0,0",
		 		 style: {
		 	         fontFamily: "'Unica One', sans-serif"
		 	      },
		 	},
	        title: {
	            text: false,
	        },
	        exporting : false,
	        credits : false,
	        subtitle: {
	            text: false,
	        },
	        xAxis: {
	            categories: dvcName,
                labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(50)
	   	           },
	            },
	        },
	        yAxis: {
	        	max : 24,
	        	step : 2,
	            title: {
	                text: false
	            },
	            labels : {
	            	style : {
	   	        	 color : "white",
	   	        	 fontSize :getElSize(30)
	   	           },
//	   	           formatter: function () {
//	   	        	   if(this.value%2==0) return this.value;
//	   	           }
	            },
	        },
	        tooltip: {
	            enabled :false
	        },
	        plotOptions: {
	            series: {
	                lineWidth: getElSize(15),
	                borderWidth: 0,
	            },
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                	formatter : function(){
	                		if(this.y!=0){
	                			return this.y;
	                		}else{
	                			return null;
	                		};
	                	},
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                        fontSize : getElSize(30),
	                        textShadow: '0 0 3px white'
	                    }
	                }
	            },
	        },
	        legend: {
	        	enabled : false
	        },
	        series: [
	                 {
	                	 data: [ 2, 18,  5, 11, 17, 22,  4,  4, 20, 14],
	                	 color : "rgb(124,224,76)"
	                 },
	                 {
	                	 data: [17,  6,  9, 10,  1,  1,  5, 14,  3,  8],
	                	 color : "yellow"
	                 },
	                 {
	                	 data: [ 5,  0, 10,  3,  6,  1, 15,  6,  1,  2],
	                	 color : "#FF3A3A"
	                 }
	                ]
	         
	    });
	 
	 Highcharts.setOptions(Highcharts.theme);
};

var block = 1/6;

function drawRect(){
	var w = getElSize(400);
	var h = getElSize(80);
	var x = (contentWidth/2) - (w/2);
	var y = $(".mainTable").height() - marginHeight;
	
	ctx.fillStyle = "white";
	ctx.fillRect(x,y,w,h);
	
	ctx.fillStyle = "black";
	ctx.font = getElSize(40) + "px Calibri";
	ctx.fillText("Reception Desk",x+getElSize(70),y + getElSize(40));
	
	ctx.fillStyle = "white";
	ctx.font = getElSize(33) + "px Calibri";
	ctx.fillText("Staff Lounge Storage",getElSize(3520), getElSize(1050) - marginHeight);
	
};

function getAllMachine(){
	var url = ctxPath + "/chart/getAllMachine.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.machineList;

			var l_x1,l_y1,l_y2,l_y2;
			var r_x1,r_y1,r_y2,r_y2;
			var t_x1,t_y1,t_x2,t_y2,t_x3,t_y3;

			var icon = "";
			var class_name = "";
			var status = "";
			$(json).each(function(idx, data){
				if(data.status==null || data.status.toLowerCase()=="in-cycle"){
					status = "in-cycle.svg";
				}else if(data.status.toLowerCase()=="wait"){ 
					status = "wait.svg";
				}else if(data.status.toLowerCase()=="alarm"){
					status = "alarm.svg";
				}else{
					status = "in-cycle.svg";
				};
				
				if(data.display==1){
					class_name = "display";
				}else{
					class_name = "hide";
				};
				icon += "<div class='init_machine_icon " + class_name + "' id='circle" + data.id + "' style='left : " + (getElSize(data.x) + marginWidth) + "; top:" + (getElSize(data.y) + marginHeight) +"; width :" + getElSize(data.w) + "';>" +  
//							"<div class='init_circle'>" + 
								//"<img src=http://factory911.com:8080/emo/machine_images/" + data.pic + ".jpg class='init_icon' id=img" + data.id + ">" +
								"<img src=" +  ctxPath + "/images/emo/"+ data.pic + "_" + status + " class='init_icon' id=img" + data.id + ">" +
//							"</div>" + 
							"<div class='machine_name' id='machine_name_" + data.id + "' style='color:white; font-size:" + getElSize(30) + "; margin-top : " + getElSize(30) + "'>" + data.name + "</div>" + 
						"</div>";
				
				if(data.id==24){
					l_x1 = (getElSize(data.x) + getElSize(data.w) + marginWidth) + getElSize(70) - marginWidth;;
					l_y1 = $(".mainTable").height() - marginHeight;
					l_x2 = (getElSize(data.x) + getElSize(data.w) + marginWidth) + getElSize(70) - marginWidth;;
					l_y2 = contentHeight - marginHeight;
					
				};
				
				if(data.id==21){
					r_x1 = (getElSize(data.x) + marginWidth) + getElSize(120) - marginWidth;
					r_y1 = $(".mainTable").height() - marginHeight;
					
					t_x1 = $("#cards").offset().left + $("#cards").width() - getElSize(10) - marginWidth;
					t_y1 = $(".mainTable").height() - marginHeight;
					t_x2 = (getElSize(data.x) + getElSize(data.w) + marginWidth) - marginWidth + getElSize(20);
					t_y2 = (getElSize(data.y) + getElSize(900)) - marginHeight;
					t_x3 = $("#cards").offset().left + $("#cards").width() - getElSize(10) - marginWidth;
					t_y3 = t_y2 + getElSize(120);
				};
				
				if(data.id==2){
					r_x2 =  (getElSize(data.x) + marginWidth) - getElSize(150)  - marginWidth;;
					r_y2 = contentHeight - marginHeight;
				};
			});
			
			drawLine(l_x1, l_y1,l_x2, l_y2);
			drawLine(r_x1, r_y1,r_x2, r_y2);
			drawTriangle(t_x1 ,t_y1, t_x2, t_y2, t_x3, t_y3);
			drawRect();
			
			$("#svg").html(icon);
			setEl();
			
			$(".init_icon").css({
				"width": "100%",
				//"margin-top" : getElSize(50),
			});
			
			$(".init_icon").each(function(idx, data){
				$(data).css({
					"left" : (circleWidth/2) - $(data).width()/2,
					"top" : (circleWidth/2) - $(data).height()/2,
				});
			});
			
			$(".init_machine_icon").draggable({
				start: function(){
				},
				stop : function(){
					var offset = $(this).offset();
		            var x = offset.left;
		            var y = offset.top;
		            var id = this.id;
		            id = id.substr(6);
		            
		    //       setInitCardPos(id, x, y);
				},
			});
			
			getMachineList();
		}
	});
	
};

function drawTriangle(x1, y1, x2, y2, x3, y3){
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.lineTo(x3,y3);
	ctx.lineTo(x1,y1);
	ctx.lineWidth = getElSize(10);
	ctx.strokeStyle = "#ffffff";
	ctx.stroke();
};

var displayMachine = new Array();
function resetIcon(){
	$(".hide").animate({
		"opacity" : 0
	},1000);
	
	reArrangeIcon();
};

function setInitCardPos(id, x, y){
	var url = ctxPath + "/chart/setInitCardPos.do";
	var param = "id=" + id + 
				"&x=" + x + 
				"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){}
	});
};

function statusBarAnim(chart, idx){
	var startPoint = $($(".highcharts-grid")[idx]).offset().left;
	var endPoint = $("#" + chart).offset().left + $("#" + chart).width()-10;
	var width = endPoint - startPoint;
	var linePos = width/24;
	
	var date = new Date();
	var hour = date.getHours();
	var minute = String(date.getMinutes());
	
	if(minute.length==1){
		minute = 0;
	}else{
		minute = minute.substr(0,1);
		minute = (linePos/6) * minute;
	};

	if(hour<20){
		linePos = (hour+4) * linePos + startPoint + minute;
	}else{
		linePos = (hour-20) * linePos + startPoint + minute;
	};
	
	$("#verticalLine" + idx).css({
		"left" : linePos,
		"height" : $("#status3_1").height()-45,
		"z-index" : 999999999,
		"display" : "inline"
	});
	
	$("#verticalLine" + idx).css("box-shadow", "0px 0px " + lineWidth + "px " + lineWidth + "px rgb(86,215,250)");

	if (toggle) {
		lineWidth -= getElSize(5)/5;
		if (lineWidth <= 0) {
			toggle = false;
		};
	} else if (!toggle) {
		lineWidth += getElSize(5)/5;
		if (lineWidth >= getElSize(5)) {
			toggle = true;
		};
	};
		
	setTimeout(function(){
		statusBarAnim(chart, idx);
	}, 80)
};

var lineWidth = getElSize(5);
var toggle = true;

var alarmList = new Array();
var machineList = new Array();
var cardMachine = new Array();

function reArrangeIcon(){
	$(cardMachine).each(function(idx, data){
		$("#circle" + data[0]).animate({
			"left" : getElSize(data[1]) + marginWidth,
			"top" : getElSize(data[2]) + marginHeight + getElSize(100)
		}, 1000, function(){
			$("#circle" + data[0] + ", #canvas").animate({
				"opacity" : 0
			});
		});
	});
	
	setTimeout(function(){
		$("#svg").hide();
		$("#cards").animate({
			"opacity" : 1
		});
		autoSlide();
		clearInterval(border_interval);
		stateBorder();
	}, 1500);
	
	getMachineStatus();
	
	//setInterval(getMachineStatus, 1000*5);
};

function setCard(){
	setEl();
	
	$(".removeIcon").css({
		"width" : getElSize(100),
		"display" : "none"
	});
	
	$(".removeIcon").click(removeCard);
	
	$(".icon").css("width" , contentWidth * 0.07 * 0.8);
	
	$(card_bar_data_array).each(function(idx, data){
		statusBar("card_status" + data.get("id"), data.get("id"));
		statusBarTotal("card_status" + data.get("id") + "_total", data.get("id"));
	});
	
	$(".machine_cam").click(viewCam);
	
	$(".icon").click(flipCard);
	
	$(".wrap").css({
		"transition" : "0.5s",
	});
	
	$(card_bar_data_array).each(function(idx, data){
		setDiagram(data.get("id"), data.get("opRatio"));
	});
};

function setDiagram(id, ratio){
	$("#diagram" + id).circleDiagram({
		textSize: getElSize(50), // text color
		percent : (ratio + 50) + "%",
		size: getElSize(170), // graph size
		borderWidth: getElSize(20), // border width
		bgFill: "#95a5a6", // background color
		frFill: "#1abc9c", // foreground color
		//font: "serif", // font
		textColor: 'black' // text color
	});
};

var machine_card;
var card_bar_data_array = [];
var new_card_bar_data_array = [];
function getMachineStatus(){
	var url = ctxPath + "/chart/getMachineStatus.do";
	
	$.ajax({
		url : url,
		dataType: "json",
		type : "post",
		success :function(data){
			var json = data.machineList;
			new_card_bar_data_array = [];
			$(json).each(function(idx, data){
				if(data.status==null || data.status.toLowerCase()=="in-cycle"){
					border_color = 'green';
				}else if(data.status.toLowerCase()=="wait"){
					border_color = 'yellow';
				}else if(data.status.toLowerCase()=="alarm"){
					border_color = 'red';
				}else if(data.status.toLowerCase()=="no-connection"){
					border_color = 'gray';
				}else{
					border_color = 'green';
				};
				
				var map = new JqMap();
				map.put("id", data.id);
				map.put("status", border_color);
				map.put("spdLoad", data.spdLoad);
				map.put("feedOverride", data.feedOverride);
				map.put("alarm", data.alarm);
				map.put("name", data.name);
				
				new_card_bar_data_array.push(map);
			});
			
			//status 변화 비교
			for(var i = 0; i < new_card_bar_data_array.length; i++){
				if(card_bar_data_array[i].get("status") != new_card_bar_data_array[i].get("status")){
					$("#wrap" + new_card_bar_data_array[i].get("id")).css("box-shadow",new_card_bar_data_array[i].get("status") + " 0px 0px " + getElSize(15) +"px " + getElSize(15) + "px");
				};
			};
			card_bar_data_array = new_card_bar_data_array;
			clearInterval(border_interval);
			stateBorder();
		}
	});
};

function getMachineList(){
	var url = ctxPath + "/chart/getEmoMachineList.do";

	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success :function(data){
			var json = data.machineList;
			var border_color = "";
			
			$(json).each(function (idx, data){
				dvcName.push(data.name);
				var array = new Array();
				array.push(data.id, data.x, data.y);
				cardMachine.push(array);
				
				var machine = new Array();
				machine.push("wrap" + data.id);
				machine.push(data.company)
				machine.push(data.name);
				machine.push(data.camIp);
				machine.push(data.x);
				machine.push(data.y);
				
				//machineList id, company, name, comIp, x, y
				machineList.push(machine);
				
				if(data.status==null || data.status.toLowerCase()=="in-cycle"){
					border_color = 'green';
				}else if(data.status.toLowerCase()=="wait"){
					border_color = 'yellow';
				}else if(data.status.toLowerCase()=="alarm"){
					border_color = 'red';
				}else if(data.status.toLowerCase()=="no-connection"){
					border_color = 'gray';
				}else{
					border_color = 'green';
				};
				
				var cam;
				if(data.camIp==null){
					cam = "<video src='http://159.122.149.132/emo/video/" + data.video + ".m4v' class='machine_cam video' autoplay loop='loop' muted='muted' id=cam" + data.id + ">";
				}else{
					cam = "<img src='http://" + data.camIp + "/mjpg/1/video.mjpg' class='machine_cam' id=cam" + data.id + " >";
				};
				
				var map = new JqMap();
				map.put("id", data.id);
				map.put("name", data.name);
				map.put("status", border_color);
				map.put("opRatio", data.id);
				
				card_bar_data_array.push(map);
				
				machine_card += "<div class='wrap' id=wrap" + data.id + " style='box-shadow : 0px 0px " + getElSize(15) + "px " + getElSize(15) + "px " + border_color + "; left : " + (getElSize(data.x) + marginWidth) + "; top : " + (getElSize(data.y) + marginHeight) + "'>" +
									"<img src="  + ctxPath + "/images/remove3.png class='removeIcon' id=r" + data.id + " style='position:absolute; z-index:10; left : " + getElSize(10) + "'>" +
									"<div class='back'>" + 
										"<img src=" + ctxPath + "/images/shopLayOut/card_back.png class='card_back'>" + 
									"</div>" + 
									"<div class='front'>" +  
										"<table  class='card_table'>" +  
											"<tr >" + 
												"<td   colspan='3'>" + 
													"<img src=" + ctxPath + "/images/company/" + data.company.toLowerCase() + ".png class='logo'> " + "<span style='float:right; font-weight:bolder; font-size:" + getElSize(40) + "'>" + data.name + "</span>" + 
												"</td>" +
											"</tr>" + 
											"<tr style='height : " + contentHeight * 0.15 + "';>" +
												"<td valign='middle' style='width:" +  getElSize(200) + "'>" + 
													"<img src=http://159.122.149.132/emo/machine_images/" + data.pic + ".jpg class='icon' id=img" + data.id + ">" + 
												"</td>" + 
												"<td align='center'>" + 
													cam +  
												"</td>" +
												"<td style='width:" +  getElSize(100) + "'><div id='diagram" + data.id + "' ></div></td>" + 
											"</tr>" +
											"<tr>" + 
												"<td colspan='3'>" +
													"<div style='position:absolute; right:0; z-index:10; font-size:" + getElSize(20) + "' class='opTime'> </div>" + 
													"<div id=card_status" + data.id + " style='width: 100%' class='card_status'></div>" +
													"<div id=card_status" + data.id + "_total style='width: 100%' class='card_status card_status_total'></div>" +
												"</td>" + 
											"</tr>" +
										"</table>" + 
									"</div>" + 
								"</div>";
				
				if(border_color=="red") alarmList.push("wrap" + data.id);
			});
			
			$("#cards").html(machine_card).css("opacity",0);
			//borderAnim();
			drawReportColumnChart("columnChart");
			setCard();
		}
	});
};

var demo_status = [];
function select_remove_card(){
	$(".removeIcon").toggle(500);
};

function removeCard(){
	var del_confirm = confirm("Are you sure you want to delete?");
	if(!del_confirm) return;
	
	var url  = ctxPath + "/chart/removeCard.do";
	var id = this.id.substr(1);
	var param = "id=" + id;
	$("#wrap" + id).toggle(500);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){}
	});
};

var hold_time = 1000;
var timeout_id = 0;
var border_flag = true;

function borderAnim(){
	var border;
	if(border_flag){
		border = getElSize(20);
	}else{
		border = 0
	};
	$(alarmList).each(function(idx, data){
		$("#" + data).css({
			"box-shadow" : "0px 0px " + border + "px " + border + "px red"
		});
	});
	
	border_flag = !border_flag;
	setTimeout(borderAnim,300);
};

function stateBorder(){
	var color = "green";
	
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("status")=="yellow"){
			color = "yellow";
		}
	};
	
	for(var i = 0; i < card_bar_data_array.length; i++){
		if(card_bar_data_array[i].get("status")=="red"){
			color = "red";
		}
	};
	
	border_interval = setInterval(function(){
		var border;
		if(border_flag){
			border = getElSize(20);
		}else{
			border = 0
		};
		
		border_flag = !border_flag;
		
		$("#stateBorder").css({
			"transition" : "0.5s",
			"box-shadow" : "inset 0px 0px " + border + "px " + border + "px " + color
		});
	}, 300);
};

var border_interval = null;
var cam_width, cam_height, cam_top, cam_left;

var cam_flag = false;


function viewCam(){
	var $cam = $("#" + this.id);
	
	if(!cam_flag){
		//var src = $cam.attr("src");
		cam_width = $cam.width();
		cam_height = $cam.height();
		cam_top = $cam.offset().top;
		cam_left = $cam.offset().left;
		
		$cam.appendTo("body");
		
		$cam.css({
			"position": "absolute",
			"top" : cam_top,
			"left" : cam_left,
			"width" : cam_width,
			"height" : cam_height,
			"z-index" : 99999
		});
			
		$cam.animate({
			"top" : 0,
			"left" : 0,
			"height" : originHeight,
			"width" : originWidth
		}, function(){
			if($cam.hasClass("video")) $cam.get(0).play();
		});
		
		//회사 로고 & 모델명
		var idx = this.id.substr(3);
		var company;
		var model_name;
		for(var i = 0; i < machineList.length; i++){
			if(machineList[i][0]=="wrap" + idx){
				company = machineList[i][1];
				model_name = machineList[i][2];
			}
		};
		
		var logo = "<img src=" + ctxPath + "/images/company/" + company.toLowerCase() + ".png class='logo_zoom' style='height:5%' >";
		var model = "<div  style='color:white;' class='model_name'>" + model_name + "</div>"; 
		$("body").prepend(logo);
		$("body").prepend(model);
		
		$(".logo_zoom").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left,
			"top" : cam_top
		});
		
		$(".model_name").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left + cam_width - $(".model_name").width(),
			"top" : cam_top,
			"font-size" : getElSize(70)
		});
		
		$(".logo_zoom").animate({
			"top" : 0,
			"left" : 0
		});
		
		$(".model_name").animate({
			"top" : 0,
			"left" : originWidth - ($(".model_name").width() + getElSize(50))
		});
		
	}else{
		closeCam($cam);
	};
	cam_flag = !cam_flag;
};

function closeCam(obj){
	$(".logo_zoom").remove();
	$(".model_name").remove();

	$(obj).animate({
		"width" : cam_width,
		"height" : cam_height,
		"top" : cam_top,
		"left" : cam_left
	}, function(){
		var id = obj.selector.substr(4);
		$("#cam" + id).appendTo("#wrap" + id + " .front .card_table tr:nth(1) td:nth(1)");
		$(obj).css("position","static");
		
		if($(obj).hasClass("video")) obj.get(0).play();
	});
};

function setCardPos(id, x, y){
	console.log(x, y)
	var url = ctxPath + "/chart/setEmoCardPos.do";
	var param = "id=" + id +  
				"&x=" + x +
				"&y=" + y;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success :function(data){
			
		}
	});
};

function statusBarTotal(id, idx){
	
	var dvc;
	for(var i = 0; i < statusColor.length; i++){
		if(statusColor[i][0]==idx){
			dvc = statusColor[i];
		};
	};

	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	
	for(var i = 0; i < dvc.length; i++){
		if(dvc[i]=="green"){
			incycle++;
		}else if(dvc[i]=="yellow"){
			wait++;
		}else if(dvc[i]=="red"){
			alarm++;
		};
	};
	
	incycle = Number(Number(incycle/6).toFixed(1));
	wait = Number(Number(wait/6).toFixed(1));
	alarm = Number(Number(alarm/6).toFixed(1));
	
	var sum = incycle + wait + alarm;

	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'green' ], [ 1, 'green' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'gray' ], [ 1, 'gray' ] ]
		}, ]
		
	
	$('#' + id).highcharts({
        chart: {
            type: 'bar',
            marginLeft:0,
            marginRight:0,
            marginTop:0,
            marginBottom:0,
          //  width : getElSize(785),
            backgroundColor : "rgba(0,0,0,0)"            	
        },
        credits : false,
        exporting : false,
        title: {
            text: ''
        },
        xAxis: {
        	labels:{
            	enabled : false
            },
            tickLength: 0
        },
        yAxis: {
            min: 0,
            max:Math.floor(sum),
            gridLineWidth: 0,
            reversedStacks: false,
            title: {
                text: ''
            },
            labels:{
            	enabled : true
            }
        },
        legend: {
        	enabled:false
        },
        plotOptions : {
        	bar: {
                stacking: 'normal',
                dataLabels: {
                	style : {
                		fontSize : getElSize(20),
                		textShadow: 0
                	},
                	enabled: true,	
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                    	var val = this.y
                    	if(val<=1){
                    		val = "";
                    	};
                    	return val;   
                    }
                }
        	},	
			series : {
				stacking : 'normal',
				pointWidth : getElSize(50),
				borderWidth : 0,
				animation : false,
				cursor : 'pointer',
			}
		},
		tooltip : {
			enabled : false
		},
        series: [{
        	color : "green",
        	data : [0]
        },
        {
        	color : colors[1],
        	data : [0]
        },
        {
        	color : colors[2],
        	data : [0]
        }]
    });
		
	
	var chart = $("#" + id).highcharts();
	
	chart.series[0].data[0].update(incycle);
	chart.series[1].data[0].update(wait);
	chart.series[2].data[0].update(alarm);
	//Number(Number(alarm/sum*10).toFixed(1))
};

var barChart
var test_options;
function statusBar(id, idx){
	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'green' ], [ 1, 'green' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'gray' ], [ 1, 'gray' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(0,0,0,0)',
				height :getElSize(50),
				margin : 0,
				marginTop : -getElSize(500)
			},
			credits : false,
			title : false,
			xAxis : {
				categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
						0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
						0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
						0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
						0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
						0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
						0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
						18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
				labels : {

					formatter : function() {
						var val = this.value
						if (val == 0) {
							val = "";
						}
						;
						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(15),
						fontWeight : "bold"
					},
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				}
			},
			legend : {
				enabled : false
			},
			series : []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		var options = status.options;

		var bar = 1/6;
		var date = new Date();
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes()));
		minute = minute.substr(0,1);
		
		if(hour>=20){
			hour -= 20;
		
		}else{
			hour += 4;
		};
		
		hour *= 6;
		
		hour+=Number(minute);
		
		var color = "";
		
		var array = [];
		array.push(idx);
		
		for(var i = 0; i < hour; i++){
			var rnd = Math.floor(Math.random()*10);
			
			if(rnd<=5){
				color = "green";
			}else if(rnd<=8){
				color = "yellow";
			}else{
				color = "red";
			};
			array.push(color);
		};
		
		statusColor.push(array);
		
		options.series = [];
		options.title = null;
		options.exporting = false;

		var dvcStatus;
		
		for(var i = 0; i < statusColor.length; i++){
			if(statusColor[i][0]==idx){
				dvcStatus = statusColor[i]; 
			}
		}
		options.series.push({
			data : [ {
				y : Number(20),
				segmentColor : dvcStatus[1]
			} ],
		});
			
		
		
		for(var i = 2; i <= dvcStatus.length; i++){
			options.series[0].data.push({
				y : Number(20),
				segmentColor : dvcStatus[i]
			});
		};
		
		for(var i = 1; i < 145-dvcStatus.length; i++){
			options.series[0].data.push({
				y : Number(20),
				segmentColor : "white"
			});
		};
		
		status = new Highcharts.Chart(options);
};

var statusColor = [];
var labelsArray = [ 20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20 ];

function flipCard_r() {
	$("#part2").append($("#mainTable2"));
	$("#back").animate({
		"width" : card_width,
		"height" : card_height,
		"top" : card_top,
		"left" : card_left,
		"background-color" : "white"
	}, function() {
		//$("#part2").append($("#mainTable2"));
		$("#back").remove();
		$(".wrap").css({
			//"transition" : "0.5s",
			"-webkit-transform" : "rotateY(0deg)",
		});
	});
};

var card_width;
var card_height;
var card_top;
var card_left;
var spd_feed_interval = null;
function flipCard() {
	var $wrap = $(this).parent().parent().parent().parent().parent().parent(); 
	var idx = this.id.substr(3);
	var img = "<img src=" + ctxPath + "/images/company/doosan.png style='height:20%'>";
	
	//spd_feed_interval = setInterval(function(){
		for(var i = 0; i < card_bar_data_array.length; i++){
			if(card_bar_data_array[i].get("id")==idx){
				console.log(idx)
				$("#machine_name").html(img + "<br>" + card_bar_data_array[i].get("name"));
				$(".neon2").html(card_bar_data_array[i].get("spdLoad"));
				$(".neon3").html(card_bar_data_array[i].get("feedOverride"));
			}
		};
	//},3000);
	
	pieChart(idx);
	drawBarChart("container", idx);
		
	$wrap.css({
		"-webkit-transform" : "rotateY(180deg)",
	});

	var div = document.createElement("div");
	div.setAttribute("id", "back");
	var top = $wrap.offset().top;
	var left = $wrap.offset().left;
	var width = $wrap.width();
	var height = $wrap.height();

	var width_padding = Number($wrap.css("padding-left").substr(0,
			$wrap.css("padding-left").lastIndexOf("px"))) * 2;
	var height_padding = Number($wrap.css("padding-top").substr(0,
			$wrap.css("padding-top").lastIndexOf("px")))
			+ Number($wrap.css("padding-bottom").substr(0,
					$wrap.css("padding-bottom").lastIndexOf("px")));

	card_width = width + width_padding;
	card_height = height + height_padding;
	card_top = $wrap.offset().top;
	card_left = $wrap.offset().left;

	div.style.cssText = "background-color:white; " + "z-index:9999999; "
			+ "position:absolute; " + "top : " + top + "; " + "left:" + left
			+ "; " + "width:" + (width + width_padding) + "; " + "height:"
			+ (height + height_padding) + ";" + "border-radius : "
			+ getElSize(30) + "px " + 
			"background-images:url(../images/shopLayOut/card_back.png";

	$("#mainTable2").css("opacity", 0);

	setTimeout(function() {
		$("body").prepend(div);
		$(div).click(flipCard_r);
		$(div).animate({
			"border-radius" : 0,
			"width" : window.innerWidth,
			"height" : window.innerHeight,
			"top" : 0,
			"left" : 0,
			"background-color" : "black",
		}, 500, function() {
			$(div).append($("#mainTable2"));
			
			for(var i = 0; i < machineList.length; i++){
				if(machineList[i][0]=="wrap" + idx)idx = i;
			};
			var machine_name = machineList[idx][2];
			var img = "<img src=" + ctxPath + "/images/company/" + machineList[idx][1].toLowerCase() + ".png style='height:20%'>";

			$("#mainTable2").animate({
				"opacity" : 1
			}, 500);
			
			$("#machine_name").css({
				"margin-top" : $("#machine_name_td").height() / 2
								- $("#machine_name").height() / 2
								- $(".subTitle").height()
			});
		});
	}, 500)
};

function hideRemoveBtn(){
	if(!rm_btn) return;	
	//$(".removeIcon").hide(500);
};

var rm_btn = false;
function evtBind() {
	cPage = $("#panel_table td img:nth(0)").attr("id");
	//$("div").not($(".wrap")).click(hideRemoveBtn);
	
	$("#menu_btn").click(togglePanel);
	$("#corver").click(function() {
		//if (panel)togglePanel();
	});
	
	$("#panel_table td img").click(slidePage);
};

var upPage = new Array();
var downPage = new Array();
var cPage;

function slidePage(){
	$("#panel_table img").css("border",0)
	$(this).css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	var id = this.id;
	
	if(cPage==id){
		$("#menu_btn").click();
		return;
	};
	
	$(".video").each(function(idx, data){
		$(this).get(0).pause();
	});
	
	if(id=="DashBoard_1"){
		$(".video").each(function(idx, data){
			$(this).get(0).play();
		});
	};
	
	upPage.push(cPage);
	var page = cPage.substr(cPage.lastIndexOf("_")+1);
	
	$("#part" + page).animate({
		"top" : - originHeight
	});
	
	cPage = id;
	
	page = id.substr(cPage.lastIndexOf("_")+1);

	$("#part" + page).animate({
		"top" : 0
	});
	
	togglePanel();
};

function showCorver(){
	$("#corver").css({
		"z-index":4,
		"opacity":0.7
	});
};

function hideCorver(){
	$("#corver").css({
		"z-index":-1,
	});
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};


function drawBarChart(id, idx) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]

	var height = window.innerHeight;

	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgb(50,50,50)',
			height : height * 0.38,
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
					0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
					0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
					0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
					0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
					0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
					0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
					18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
			labels : {

				formatter : function() {
					var val = this.value
					if (val == 0) {
						val = "";
					};
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(15),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#container").highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;

	var mahcine_status;
	
	for(var i = 0; i < statusColor.length; i++){
		if(statusColor[i][0]==idx){
			mahcine_status = statusColor[i] 
		}
	};
	
	options.series.push({
		data : [ {
			y : Number(20),
			segmentColor : mahcine_status[1]
		} ],
	});
		
	
//	var date = new Date();
//	var hour = date.getHours();
//	var minute = String(date.getMinutes());
//	minute = minute.substr(0,1);
//	
//	if(hour>=20){
//		hour -= 20;
//	
//	}else{
//		hour += 4;
//	};
//	
//	hour *= 6;
//	
//	hour+=Number(minute);
	
	
	for(var i = 1; i < mahcine_status.length; i++){
		options.series[0].data.push({
			y : Number(20),
			segmentColor : mahcine_status[i]
		});
	};
	
	for(var i = 1; i < 145-mahcine_status.length; i++){
		options.series[0].data.push({
			y : Number(20),
			segmentColor : "rgba(0,0,0,0)"
		});
	};
	

	status = new Highcharts.Chart(options);
};

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':'
			+ sec;
	return time;
}

function pieChart(idx) {
	Highcharts.setOptions({
		// green yellow red gray
		colors : [ 'rgb(100,238,92 )', 'rgb(250,210,80 )', 'rgb(231,71,79 )',
				'#8C9089' ]
	});

	$('#pie')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',
							backgroundColor : "rgba(0,0,0,0)"
						},
						credits : false,
						exporting : false,
						title : {
							text : false
						},
						legend : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'white',
										fontSize : getElSize(30),
										textShadow: 0
									}
								},
								showInLegend : true
							}
						},
						series : [ {
							name : "Brands",
							colorByPoint : true,
							data : [ {
								name : "In-Cycle",
								y : 0
							}, {
								name : "Wait",
								y : 0
							}, {
								name : "Alarm",
								y : 0
							}]
						} ]
					});
	
	
	var dvc;
	for(var i = 0; i < statusColor.length; i++){
		if(statusColor[i][0]==idx){
			dvc = statusColor[i];
		};
	};

	var incycle = 0;
	var wait = 0;
	var alarm = 0;
	
	for(var i = 0; i < dvc.length; i++){
		if(dvc[i]=="green"){
			incycle++;
		}else if(dvc[i]=="yellow"){
			wait++;
		}else if(dvc[i]=="red"){
			alarm++;
		};
	};
	
	var chart = $("#pie").highcharts();
	
	var sum = incycle + wait + alarm;
	
	chart.series[0].data[0].update(Number(Number(incycle/sum*100).toFixed(1)));
	chart.series[0].data[1].update(Number(Number(wait/sum*100).toFixed(1)));
	chart.series[0].data[2].update(Number(Number(alarm/sum*100).toFixed(1)));
};

function setEl() {
	$("#canvas").css({
		"position" : "absolute",
		"background" : "whtie",
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$("#stateBorder").css({
		"position": "absolute",
		"width": contentWidth,
		"height" : contentHeight,
		"top" : marginHeight,
		"left" : marginWidth,
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$(".machine_icon, .init_machine_icon").css({
		"position" : "absolute",
		"margin" : getElSize(20),
//		"width" : contentWidth * 0.1,
//		"height" : contentWidth * 0.1,
		"cursor" : "pointer"
	});

	$(".circle, .init_circle").css({
		"background-color" : "white",
		"width" : contentWidth * 0.07,
		"height" : contentWidth * 0.07,
		"border-radius" : "50%",
	});
	
	// 페이지 위치 조정
//	for (var i = 2; i <= 5; i++) {
//		$("#part" + i).css({
//			"top" : $("#part" + (i - 1)).height() + originHeight
//		});
//	};

	$(".page").css({
		"top" : originHeight
	});
	
	$("#part1").css({
		"top" : 0
	});
	
	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(70)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1,
		"margin-left" : getElSize(150)
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"width" : contentWidth * 0.04,
		"top" : marginHeight + (contentHeight * 0.01),
		"left" : getElSize(30),
		"z-index" : 5
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 999999,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(60)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});


	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$(".wrap").css({
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		"margin" : getElSize(50)
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	var chart_height = getElSize(60);
	if(chart_height<20) chart_height = 20;
	
	$(".card_status").css({
		"height" : chart_height,
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.015
	});
	
	$(".menu_icon").css({
		"width" : getElSize(250),
		"background-color" : "white",
		"border-radius" : "50%"
	});

	$("p").css({
		"font-size" : getElSize(30),
		"font-weight" : "bolder"
	});
	
	$(".mainTable").not("#mainTable").css({
		"border-spacing" : getElSize(30)
	});
	
	$(".td_header").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"background-color" : "rgb(34,34,34)",
		"padding" : getElSize(20),
		"margin" : getElSize(10)
	});
	
	$("#reportTable .span").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170)
	});
	
	$(".upDown").css({
		"width" : getElSize(100),
		"margin-top" : getElSize(100)
	});
	
	$("#upFont").css({
		"color" : "rgb(124,224,76)",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#downFont").css({
		"color" : "#FF3A3A",
		"font-size" : getElSize(100),
		"margin-left" : getElSize(50)
	});
	
	$("#thermometer").css({
		"width" : getElSize(130),
		"height" : getElSize(500),
		"margin-bottom" : getElSize(30),
		"border-radius" : getElSize(35),
		"margin- top" : getElSize(50)
	});
	
	$("#thermometer .track").css({
		"width" : getElSize(30),
		"height" : getElSize(450),
		"top" : getElSize(25),
	});
	
	$("#thermometer .progress .amount").css({
		"padding": 0 + " " + getElSize(170) + " 0 " + getElSize(30),
		"font-size" : getElSize(50)
	});
	
	$("#diagram").css({
		"margin-top" : getElSize(50)
	});
	
	$("#reportDateDiv").css({
		"position" : "absolute",
		"right" : getElSize(50),
	});
	
	$("#sDate, #eDate").css({
//		"width" : getElSize(400),
//		"height" : getElSize(50),
		"font-size" : getElSize(40)
	});
	
	$("#map").css({
		"left" : (originWidth/2) - ($("#map").width()/2) - marginWidth 
	});
	
	$("#comName").css({
		"font-size" : getElSize(70)
	});
	
	$("#reportTable, #mainTable2").css("margin-top",$("#mainTable").offset().top);
};

function drawStockChart() {
	var seriesOptions = [], seriesCounter = 0, names = [ 'MSFT', 'AAPL', 'GOOG' ],
	// create the chart when all data is loadedx
	createChart = function() {
		$('#container').highcharts('StockChart', {
			chart : {
				height : originHeight * 0.45
			},
			exporting : false,
			credits : false,
			rangeSelector : {
				selected : 4
			},

			rangeSelector : {
				buttons : [ {
					type : 'hour',
					count : 1,
					text : '1h'
				}, {
					type : 'day',
					count : 1,
					text : '1d'
				}, {
					type : 'month',
					count : 1,
					text : '1m'
				}, {
					type : 'year',
					count : 1,
					text : '1y'
				}, {
					type : 'all',
					text : 'All'
				} ],
				inputEnabled : true, // it supports only days
				selected : 4
			// all
			},

			yAxis : {
				labels : {
					formatter : function() {
						// return (this.value > 0 ? ' + ' : '') + this.value +
						// '%';
						return this.value
					}
				},
				plotLines : [ {
					value : 0,
					width : 2,
					color : 'silver'
				} ]
			},

			/*
			 * plotOptions: { series: { compare: 'percent' } },
			 */

			tooltip : {
				// pointFormat: '<span
				// style="color:{series.color}">{series.name}</span>:
				// <b>{point.y}</b> ({point.change}%)<br/>',
				valueDecimals : 2
			},

			series : seriesOptions
		});
	};

	seriesOptions[0] = {
		name : names[0],
		data : data_
	};

	/*
	 * $.each(names, function (i, name) {
	 * 
	 * $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' +
	 * name.toLowerCase() + '-c.json&callback=?', function (data) {
	 * seriesOptions[i] = { name: name, data: data_ };
	 * 
	 * seriesCounter += 1;
	 * 
	 * if (seriesCounter === names.length) { createChart(); } }); });
	 */

	Highcharts.createElement('link', {
		href : '//fonts.googleapis.com/css?family=Unica+One',
		rel : 'stylesheet',
		type : 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
		colors : [ "#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee",
				"#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF",
				"#aaeeee" ],
		chart : {
			backgroundColor : {
				linearGradient : {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 1
				},
				stops : [ [ 0, '#2a2a2b' ], [ 1, '#3e3e40' ] ]
			},
			style : {
				fontFamily : "'Unica One', sans-serif"
			},
			plotBorderColor : '#606063'
		},
		title : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase',
				fontSize : '20px'
			}
		},
		subtitle : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase'
			}
		},
		xAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			title : {
				style : {
					color : '#A0A0A3'

				}
			}
		},
		yAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			tickWidth : 1,
			title : {
				style : {
					color : '#A0A0A3'
				}
			}
		},
		tooltip : {
			backgroundColor : 'rgba(0, 0, 0, 0.85)',
			style : {
				color : '#F0F0F0'
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					color : '#B0B0B3'
				},
				marker : {
					lineColor : '#333'
				}
			},
			boxplot : {
				fillColor : '#505053'
			},
			candlestick : {
				lineColor : 'white'
			},
			errorbar : {
				color : 'white'
			}
		},
		legend : {
			itemStyle : {
				color : '#E0E0E3'
			},
			itemHoverStyle : {
				color : '#FFF'
			},
			itemHiddenStyle : {
				color : '#606063'
			}
		},
		credits : {
			style : {
				color : '#666'
			}
		},
		labels : {
			style : {
				color : '#707073'
			}
		},

		drilldown : {
			activeAxisLabelStyle : {
				color : '#F0F0F3'
			},
			activeDataLabelStyle : {
				color : '#F0F0F3'
			}
		},

		navigation : {
			buttonOptions : {
				symbolStroke : '#DDDDDD',
				theme : {
					fill : '#505053'
				}
			}
		},

		// scroll charts
		rangeSelector : {
			buttonTheme : {
				fill : '#505053',
				stroke : '#000000',
				style : {
					color : '#CCC'
				},
				states : {
					hover : {
						fill : '#707073',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					},
					select : {
						fill : '#000003',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					}
				}
			},
			inputBoxBorderColor : '#505053',
			inputStyle : {
				backgroundColor : '#333',
				color : 'silver'
			},
			labelStyle : {
				color : 'silver'
			}
		},

		navigator : {
			handles : {
				backgroundColor : '#666',
				borderColor : '#AAA'
			},
			outlineColor : '#CCC',
			maskFill : 'rgba(255,255,255,0.1)',
			series : {
				color : '#7798BF',
				lineColor : '#A6C7ED'
			},
			xAxis : {
				gridLineColor : '#505053'
			}
		},

		scrollbar : {
			barBackgroundColor : '#808083',
			barBorderColor : '#808083',
			buttonArrowColor : '#CCC',
			buttonBackgroundColor : '#606063',
			buttonBorderColor : '#606063',
			rifleColor : '#FFF',
			trackBackgroundColor : '#404043',
			trackBorderColor : '#404043'
		},

		// special colors for some of the
		legendBackgroundColor : 'rgba(0, 0, 0, 0.5)',
		background2 : '#505053',
		dataLabelsColor : '#B0B0B3',
		textColor : '#C0C0C0',
		contrastTextColor : '#F0F0F3',
		maskColor : 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	createChart();
};