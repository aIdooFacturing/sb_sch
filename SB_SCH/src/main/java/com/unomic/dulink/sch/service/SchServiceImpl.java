package com.unomic.dulink.sch.service;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.sch.domain.ParamVo;

@Service
@Repository
public class SchServiceImpl implements SchService{

	private final static String SCH_SPACE= "com.unos.sch.";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SchServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	@Transactional//(value="txManager_ma")
	public String addDvcSmry(ParamVo prmVo)
	{
		LOGGER.info("RunAddDvcSmry");
		LOGGER.info("prmVo.getDate():"+prmVo.getDate());
		try {
			sql_ma.insert(SCH_SPACE + "addDvcSmry", prmVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}
	
	@Override
	@Transactional//(value="txManager_ma")
	public String addDvcTmChart(ParamVo prmVo)
	{
		try {
			sql_ma.insert(SCH_SPACE + "addDvcTmChart", prmVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}
	
	public String addDvcLast(){
		sql_ma.insert(SCH_SPACE+"addDvcLast");
		return "OK";
	}
	
	public String setNoCon(ParamVo prmVo){
		sql_ma.insert(SCH_SPACE+"setNoCon", prmVo);
		return "OK";
	}
	
	@Override
	@Transactional
	public String addNightStatus(ParamVo prmVo){
		try {
			sql_ma.insert(SCH_SPACE+"addNightStatus", prmVo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}
	
	public String editDvcLast(){
		LOGGER.info("RUN editDvcLast");
		sql_ma.update(SCH_SPACE+"editDvcLast");
		return "OK";
	}
	
	public String editDvcLastStatus(){
		LOGGER.info("RUN editDvcLastStatus");
		sql_ma.update(SCH_SPACE+"editDvcLastStatus");
		return "OK";
	}
	
	public String bkOldAdtStatus(){
		LOGGER.info("RUN bkOldAdtStatus");
		sql_ma.insert(SCH_SPACE+"bkOldAdtStatus");
		return "OK";
	}
	
}