package com.unomic.dulink.sch.service;

import com.unomic.dulink.sch.domain.ParamVo;

public interface SchService {
	public String addDvcSmry(ParamVo prmVo);
	public String addDvcLast();
	public String editDvcLast();
	public String editDvcLastStatus();
	public String addDvcTmChart(ParamVo prmVo);
	public String setNoCon(ParamVo prmVo);
	public String addNightStatus(ParamVo prmVo);
	public String bkOldAdtStatus();
}
