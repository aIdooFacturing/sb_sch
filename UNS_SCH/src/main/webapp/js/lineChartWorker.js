var prog_run = false;
onmessage = function(evt){
	var data = evt.data;
	prog_run = true;
	if(data=="done"){
		prog_run = false;
	}else{
		sendData();
	};
};

function sendData(){
	var x = (new Date()).getTime(); // current time
	var y = 20 + (Math.random());
	
	var data = [x,y];
	postMessage(data);
	if(prog_run){
		setTimeout(function(){
			sendData();
		}, 1000)
	};
};