$(function() {
	getIconList();
	getToday();
	setEl();
	setTimeout(dateSlide, 2000);
	//pdfViewer();
	evtBind();
});

function evtBind(){
	$("#close_btn").click(closePdf);
};

function dateSlide(){
	$("#today").animate({
		"top" : -$("#today").height()
	});
	
	$("#iconArticle").animate({
		"top" : originHeight/2 - $("#iconArticle").height()/2 
	});
};

function closePdf(){
	$("iframe").attr("src", "");
	$("iframe").css({
		"z-index" : -1,
	});
	$("#close_btn").css("display","none");
	$("#loader").css("display", "none");
};

function pdfViewer(){
	var src = "http://docs.google.com/gview?url=http://106.240.234.114:8080/emo/sample.pdf&embedded=true";
	$("#loader").css("display", "inline");
	$("iframe").attr("src", src);
	$("iframe").css({
		"z-index" : 10,
	});
	$("#close_btn").css("display","inline");
}

function getToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
		
	var yearDiv = document.createElement("div");
	var yearText = document.createTextNode(year + "/" + month + "/" + day)
	yearDiv.setAttribute("id", "yearDiv");

	var timeDiv = document.createElement("div");
	var timeText = document.createTextNode(hour + ":" + minute)
	timeDiv.setAttribute("id", "timeDiv");
	
	yearDiv.appendChild(yearText);
	timeDiv.appendChild(timeText);
	
	$(yearDiv).css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(600)
	});
	
	$(timeDiv).css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(300),
		"float" : "right"
	});
	
	$("#today").append(yearDiv);
	$("#today").append(timeDiv);
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function getIconList(){
	var url = ctxPath + "/chart/getEmoMachineList.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.machineList;
			
			$(json).each(function(idx, data){
				var icon = document.createElement("div");
				icon.setAttribute("id", "machine" + data.id);
				var div = document.createElement("div");
				var img = document.createElement("img");
				img.setAttribute("src","http://factory911.com:8080/emo/machine_images/" + data.pic + ".jpg");
				
				var nameDiv = document.createElement("span");
				var nameNode = document.createTextNode(data.name);
				nameDiv.appendChild(nameNode);
				
				$(nameDiv).css({
					"color" : "white",
					"font-size" : 25
				});
				
				$(div).css({
					"width" : originWidth * 0.2,
					"height" : originWidth * 0.2,
					"background-color" : "white",
					"border-radius" : "50%",
					"overflow" : "hidden",
					"float" : "left",
				});
				
				$(icon).css({
					"width" : originWidth * 0.2,
//					"height" : originWidth * 0.2,
					"float" : "left",
					"margin" : getElSize(50) + " " +  getElSize(100) + " " +  getElSize(50) + " " +  getElSize(100)
				});
				
				div.appendChild(img);
				
				icon.appendChild(div);
				icon.appendChild(nameDiv);
				
				$(img).css({
					"width" : "90%",
					"height" : "90%"
				});
				
				$(icon).click(showJobList);
				$("#iconArticle").append(icon);
				
				$("#iconArticle").css({
					width : (originWidth * 0.2 *3) + (getElSize(200)*3)
				});
				
				$("#iconArticle").css({
					left : (originWidth/2) - ($("#iconArticle").width()/2),
				});
			});
		}
	});
};

function showJobList(){
//	location.href = ctxPath + "/prog/playList.do";
	var id = this.id.substr(7);
	location.href = ctxPath + "/prog/playList.do?id=" + id;
//	var name = $(this).children("span").html()
//	$("#iconArticle").toggle();
//	
//	var table = "<article>" + 
//					"<p style='color:white; font-size:" + getElSize(150) + "'>" + name + "</p>" + 
//					"<input type='date' id='date' style='font-size:" + getElSize(150) + "'/>" + 
//					
//					"<table style = 'color:white; border-collapse: collapse;' id='jobTable'>" + 
//						"<tr>" + 
//							"<td width='10%' rowspan='3'>1</td>" +
//							"<td align='center' colspan='3'><div class='td_header'>Job Title</div></td>" +
//						"</tr>" +
//						"<tr>" + 
//							"<td>Prog. Name</td>" +
//							"<td width='10%'><img src=" + ctxPath + "/images/upload.png  onclick='upLoad()'></td>" + 
//							"<td width='10%'><img src=" + ctxPath + "/images/download.png onclick='downLoad()'></td>" + 
//						"</tr>" + 
//						"<tr>" + 
//							"<td colspan='4'>Description</td>" + 
//						"</tr>" +
//						"<tr>" + 
//							"<td width='10%' rowspan='3'>2</td>" +
//							"<td align='center' colspan='3'>Job Title</td>" +
//						"</tr>" +
//						"<tr>" + 
//							"<td>Prog. Name</td>" +
//							"<td width='10%'><img src=" + ctxPath + "/images/upload.png onclick='upLoad()'></td>" + 
//							"<td width='10%'><img src=" + ctxPath + "/images/download.png onclick='downLoad()'></td>" + 
//						"</tr>" + 
//						"<tr>" + 
//							"<td colspan='4'>Description</td>" + 
//						"</tr>" +
//					"</table>" +
//				"</article>";
//	
//	$("#iconArticle").parent().append(table);
//	
//	$("#jobTable").css({
//		"width" : "90%",
//		"margin" : originWidth * 0.005,
//		"font-size" : getElSize(200),
//		"border-spacing" : 10
//	});
//	
//	$("#jobTable td").css({
//		"border" : "3px solid white",
//		"padding" : 20,
//	});
//	
//	$("#jobTable td").css({
//		"background-color" : "rgb(47,47,47)",
//		"padding" : 10
//	});
//	
//	$(".td_header").css({
//		"background-color" : "rgb(34,34,34)",
//		"padding" : 10,
//	});
//	
//	var date = new Date();
//	var year = date.getFullYear();
//	var month = addZero(String(date.getMonth() + 1));
//	var day = addZero(String(date.getDate()));
//	
//	$("#date").val(year + "-" + month + "-" + day);
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

function downLoad(){
	pdfViewer();
};

function upLoad(){
	alert("upLoad!")
};

function setEl() {
	$("#today").css({
		"position" : "absolute",
		"top" : (originHeight/2) - ($("#today").height()/2) - (getElSize(300)),
	});
	
	$("#today").css({
		"position" : "absolute",
		"left" : (originWidth/2) - ($("#today").width()/2)
	});
	
	$("#iconArticle").css({
		"position" : "absolute",
		"top" : originHeight
	});
	
	$("iframe").css({
		"position" : "absolute",
		"width" : "90%",
		"height" : "90%",
		"z-index" : -1
	});
	
	$("iframe").css({
		"left" : (originWidth/2) - ($("iframe").width()/2),
		"top" : (originHeight/2) - ($("iframe").height()/2)
	});
	
	$("#close_btn").css({
		"position" : "absolute",
		"background-color" : "white",
		"border-radius" : "50%",
		"width" : "5%",
		"display" : "none",
		"z-index" : 11
	});
	
	$("#close_btn").css({
		"left" : $("iframe").offset().left - ($("#close_btn").width()/2),
		"top" : $("iframe").offset().top - ($("#close_btn").height()/2)
	});
	
	$("#loader").css({
		"position" : "absolute",
		"left" : (originWidth/2) - ($("#loader").width()/2),
		"top" : (originHeight/2) - ($("#loader").height()/2),
		"z-index" : 9,
		"display" : "none"
 	});
};